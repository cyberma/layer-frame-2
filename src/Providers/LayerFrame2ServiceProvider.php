<?php
/**
 * Created by Cyberma s.r.o.
 * Web: www.cyberma.net
 * User: Peter Matisko
 * Date: 07.02.2021
 */

namespace Cyberma\LayerFrame2\Providers;

use Cyberma\LayerFrame2\ApiMappers\ApiMapper;
use Cyberma\LayerFrame2\Contracts\ApiMappers\IApiMapper;
use Cyberma\LayerFrame2\Contracts\DBMappers\IDBMapper;
use Cyberma\LayerFrame2\Contracts\DBStorage\IDBStorage;
use Cyberma\LayerFrame2\Contracts\Errors\IErrorBag;
use Cyberma\LayerFrame2\Contracts\ModelMaps\IModelMap;
use Cyberma\LayerFrame2\Contracts\Models\IModelFactory;
use Cyberma\LayerFrame2\Contracts\Pagination\IPaginator;
use Cyberma\LayerFrame2\Contracts\Pagination\ITableSearcher;
use Cyberma\LayerFrame2\Contracts\Repositories\IRepository;
use Cyberma\LayerFrame2\Contracts\InputParsers\IInputParser;
use Cyberma\LayerFrame2\DBMappers\DBMapper;
use Cyberma\LayerFrame2\DBStorage\DBStorage;
use Cyberma\LayerFrame2\Errors\ErrorBag;
use Cyberma\LayerFrame2\Exceptions\ExceptionHandler;
use Cyberma\LayerFrame2\InputParsers\InputParser;
use Cyberma\LayerFrame2\ModelMaps\ModelMap;
use Cyberma\LayerFrame2\Pagination\InputModels\PaginatorInput;
use Cyberma\LayerFrame2\Pagination\InputModels\SearcherInput;
use Cyberma\LayerFrame2\Pagination\Paginator;
use Cyberma\LayerFrame2\Pagination\TableSearcher;
use Cyberma\LayerFrame2\Repositories\Repository;
use Illuminate\Support\ServiceProvider;


class LayerFrame2ServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->registerGeneralObjects();
        $this->registerInterfaceObjects();
        $this->registerDBObjects();
    }


    private function registerGeneralObjects()
    {

        $this->app->singleton(ExceptionHandler::class, function () {
            return new ExceptionHandler();
        });

        $this->app->bind(IErrorBag::class, function () {
            return new ErrorBag();
        });

        $this->app->singleton(IInputParser::class, function () {
            return new InputParser();
        });

        $this->app->singleton(IApiMapper::class, function () {
            return new ApiMapper();
        });

    }


    private function registerInterfaceObjects()
    {
        $this->app->singleton(ITableSearcher::class, function() {
            return new TableSearcher(new InputParser(), new SearcherInput());
        });

        $this->app->singleton(IPaginator::class, function() {
            return new Paginator(new InputParser(), new PaginatorInput());
        });
    }


    private function registerDBObjects()
    {
        $this->app->bind(IModelMap::class, ModelMap::class);
        $this->app->bind(IDBStorage::class, function($app, array $params) {
            return new DBStorage($params['modelMap']);
        });
        $this->app->bind(IDBMapper::class, function($app, array $params) {
            return $app->make(DBMapper::class, $params);
        });
        $this->app->bind(IRepository::class, function($app, array $params) {
            $dbStorage = array_key_exists('dbStorage', $params) ? $params['dbStorage'] : $app->make(IDBStorage::class, $params);
            $dbMapper = array_key_exists('dbMapper', $params) ? $params['dbMapper'] : $app->make(IDBMapper::class, $params);
            $modelMap = $params['modelMap'];

            return new Repository($dbStorage, $dbMapper, $modelMap);
        });
    }
}
