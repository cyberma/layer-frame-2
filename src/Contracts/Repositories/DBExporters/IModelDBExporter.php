<?php
/**
 * Created by Cyberma s.r.o.
 * Web: www.cyberma.net
 * User: Peter Matisko
 * Date: 22.02.2021
 */

namespace Cyberma\LayerFrame2\Contracts\Repositories\DBExporters;

use Cyberma\LayerFrame2\Contracts\Models\IModel;


interface IModelDBExporter
{
    public function exportModel(IModel $model, array $whichAttributes = [], array $except = []): array;
}
