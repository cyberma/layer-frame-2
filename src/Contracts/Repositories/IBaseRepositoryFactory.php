<?php
/**
 * Created by Cyberma s.r.o.
 * Web: www.cyberma.net
 * User: Peter Matisko
 * Date: 22.02.2021
 */

namespace Cyberma\LayerFrame2\Contracts\Repositories;

use Cyberma\LayerFrame2\Contracts\DBMappers\IDBMapper;
use Cyberma\LayerFrame2\Contracts\DBStorage\IDBStorage;
use Cyberma\LayerFrame2\Contracts\ModelMaps\IModelMap;


interface IBaseRepositoryFactory
{
    /**
     * @param IDBStorage $dbStorage
     * @param IDBMapper $dbMapper
     * @param IModelMap $modelMap
     * @return \Cyberma\LayerFrame2\Contracts\Repositories\IRepository
     */
    public function createRepository(IDBStorage $dbStorage, IDBMapper $dbMapper, IModelMap $modelMap): IRepository;
}
