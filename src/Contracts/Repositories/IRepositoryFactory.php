<?php
/**
 * Created by Cyberma s.r.o.
 * Web: www.cyberma.net
 * User: Peter Matisko
 * Date: 22.02.2021
 */

namespace Cyberma\LayerFrame2\Contracts\Repositories;


interface IRepositoryFactory
{
    public function createRepository(): IRepository;
}
