<?php
/**
 * Created by Cyberma s.r.o.
 * Web: www.cyberma.net
 * User: Peter Matisko
 * Date: 23.04.2021
 */

namespace Cyberma\LayerFrame2\Contracts\ApiMappers;

use Cyberma\LayerFrame2\Contracts\Models\IModel;

interface IApiMapper
{
    public function mapModelToApi(IModel $model, array $apiMap): array;
}
