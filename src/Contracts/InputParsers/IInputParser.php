<?php
/**
 * Created by Cyberma s.r.o.
 * Web: www.cyberma.net
 * User: Peter Matisko
 * Date: 22.03.2021
 */

namespace Cyberma\LayerFrame2\Contracts\InputParsers;

use Cyberma\LayerFrame2\Contracts\InputModels\IInputModel;

interface IInputParser
{
    /**
     * @param IInputModel $inputModel
     * @param array $requestData
     * @param string $validatedSet
     * @param array $additionalInputs
     * @return IInputModel
     * @throws \Cyberma\LayerFrame2\Exceptions\Exception
     */
    public function parse(IInputModel $inputModel, array $requestData, string $validatedSet, array $additionalInputs = []): IInputModel;
}
