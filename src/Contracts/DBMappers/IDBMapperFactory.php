<?php
/**
 * Created by Cyberma s.r.o.
 * Web: www.cyberma.net
 * User: Peter Matisko
 * Date: 27.02.2021
 */

namespace Cyberma\LayerFrame2\Contracts\DBMappers;


interface IDBMapperFactory
{
    /**
     * @return IDBMapper
     */
    public function createDBMapper(): IDBMapper;
}
