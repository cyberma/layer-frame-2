<?php
/**
 * Created by Cyberma s.r.o.
 * Web: www.cyberma.net
 * User: Peter Matisko
 * Date: 22.03.2021
 */

namespace Cyberma\LayerFrame2\Contracts\Pagination;

interface ITableSearcher
{
    public static function getAllowedSearchOperators();

    /**
     * @param int $page
     * @param int $perPage
     * @param string $sortBy
     * @param string $sortDirection
     */
    public function setSearcher(string $searchAt, string $searchFor, string $operator = 'eq');

    /**
     * @return array
     */
    public function getConditions(): array;
}
