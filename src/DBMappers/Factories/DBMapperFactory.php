<?php
/**
 * Created by Cyberma s.r.o.
 * Web: www.cyberma.net
 * User: Peter Matisko
 * Date: 21.02.2021
 */

namespace Cyberma\LayerFrame2\DBMappers\Factories;

use Cyberma\LayerFrame2\Contracts\DBMappers\IDBMapperFactory;
use Cyberma\LayerFrame2\Contracts\ModelMaps\IModelMap;
use Cyberma\LayerFrame2\Contracts\Models\IModelFactory;
use Cyberma\LayerFrame2\Contracts\Repositories\DBExporters\IModelDBExporter;
use Cyberma\LayerFrame2\DBMappers\DBMapper;


class DBMapperFactory implements IDBMapperFactory
{
    /**
     * @var IModelMap
     */
    private IModelMap $modelMap;
    /**
     * @var IModelFactory
     */
    private IModelFactory $modelFactory;

    /**
     * DBMapperFactory constructor.
     * @param IModelMap $modelMap
     * @param IModelFactory $modelFactory
     * @param IModelDBExporter $modelDBExporter
     */
    public function __construct(IModelMap $modelMap, IModelFactory $modelFactory)
    {

        $this->modelMap = $modelMap;
        $this->modelFactory = $modelFactory;
    }

    /**
     * @return DBMapper
     */
    public function createDBMapper(): DBMapper
    {
        return new DBMapper($this->modelMap, $this->modelFactory);
    }
}
